from django.apps import AppConfig


class FrontendTestConfig(AppConfig):
    name = 'apps.frontend_test'
